/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ketan
 */
public class ProblemB {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for(int c_i=0;c_i<T;c_i++)
        {
            int n = sc.nextInt();
            int[][] a = new int[(n*n)][2];int index=0,iresult=0,count=0;
            int[] result = new int[n];
            ArrayList<Integer> arr = new ArrayList<Integer>();
            for(int i=0;i<(2*n-1);i++)
            {
              
                for(int j=0;j<n;j++)
                {
                    int temp = sc.nextInt();
                    if(count==0)
                    {
                        arr.add(temp);
                        a[index][0]=temp;
                        index++;
                        count++;
                    }
                    if(!arr.contains(temp))
                    {
                        arr.add(temp);
                        a[index][0] = temp;
                        a[index][1]++;
                        index++;
                    }
                    else
                    {
                        int t=0;
                        while(t<index)
                        {
                            if(a[t][0]==temp)
                            {
                                a[t][1]++;
                                break;
                            }
                            t++;
                        }
                    }
                }
            }
            for(int i=0;i<(n*n);i++)
            {
                if((a[i][1]%2)==1)
                {
                    //System.out.println(a[i][0]);
                    result[iresult]=a[i][0];
                    iresult++;
                }
                    
            }
            for(int i=0;i<(n-1);i++)
            {
                for(int j=(i+1);j<n;j++)
                {
                    int temp = result[i];
                    if(temp>result[j])
                    {
                        result[i] = result[j];
                        result[j] = temp;
                        temp = result[i];
                    }
                }
            }
            String res = "Case #"+(c_i+1)+": ";
            for(int i=0;i<n;i++)
            {
              res = res+result[i]+" ";
            }
            System.out.println(res);
            
        }
            
    }
    
}
