/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ketan
 */
public class ProblemC {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for(int T_i=0;T_i<T;T_i++)
        {   
            int N= sc.nextInt();
            int[] arr = new int[(N+1)];
            ArrayList<Integer> result = new ArrayList<Integer>();
            int temp =1,count=1,finalCount=0 ;boolean finish = false;
            for(int N_i=1;N_i<(N+1);N_i++)
            {
                arr[N_i] = sc.nextInt();
            }
            for(int a =1;a<N;a++)
            {
                count=1;
                temp = a;
                result.add(temp);
                temp = arr[temp];
                while(!result.contains(temp))
                {
                   // System.out.println("adding "+temp);
                    result.add(temp);
                    temp = arr[temp];
                    count++;
                }
                temp = result.get((result.size()-1));
                while(!finish)
                {
                    int update=0;
                    for(int i=1;i<(N+1);i++)
                    {
                        if((arr[i]==temp) && (!result.contains(i)))
                        {
                            //System.out.println(temp+"  "+result.contains(i));
                            temp  = i;
                            while(!result.contains(temp))
                            {
                                //System.out.println("Inside for loop adding "+temp);
                                result.add(temp);
                                temp = arr[temp];
                                count++;
                                update++;
                            }
                            temp = result.get((result.size()-1));
                            
                        }
                        if(update==0 && i==N)
                        {
                            finish = true;
                        }
                    }
                }
            //System.out.println("count is "+count);
                if(!(count>2 && arr[temp]==a))
                {
                    for(int i=2;i<(N+1);i++)
                    {
                        if((arr[i]==1) &&(!result.contains(i)))
                        {
                            count++;
                         break;
                        }
                    }
                }
                if(count>finalCount)
                {
                    finalCount = count;
                }
                
            }  
            System.out.println("Case #"+(T_i+1)+": "+finalCount);
            
        }
    
    }
}
