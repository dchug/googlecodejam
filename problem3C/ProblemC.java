/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ketan
 */
public class ProblemC {

    /**
     * @param args the command line arguments
     */
    private static int getIndex(ArrayList<String> a,String s)
    {
        int index =-1;
        if(a.contains(s)){
        for(int i=0;i<a.size();i++)
        {
            if(s.equals(a.get(i)))
                index =i;
        }
        }
        return index;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for(int T_i=0;T_i<T;T_i++)
        {
            int J = sc.nextInt();
            int P = sc.nextInt();
            int S = sc.nextInt();
            int K = sc.nextInt();
            
            ArrayList<String> jp = new ArrayList<String>();
            ArrayList<String> js = new ArrayList<String>();
            ArrayList<String> ps = new ArrayList<String>();
            int[] count_jp = new int[K];
            int[] count_js = new int[K];
            int[] count_ps = new int[K];
            ArrayList<String> result = new ArrayList<String>();
            int jp_i=0,ps_i=0,js_i=0,count=0,check=0;
            
            for(int j =1;j<(J+1);j++)
            {
                for(int p =1;p<(P+1);p++)
                {
                    for(int s =1;s<(S+1);s++)
                    {
                        if(((!jp.contains(j+""+p))) && (!js.contains(j+""+s))
                                && (!ps.contains(p+""+s)))
                        {
                            jp.add(j+""+p);
                            count_jp[jp_i]=1;
                            jp_i++;
                                               
                            js.add(j+""+s);
                            count_ps[ps_i]=1;
                            ps_i++;
                            
                            ps.add(p+""+s);
                            count_js[js_i]=1;
                            js_i++;
                            
                            result.add(j+" "+p+" "+s);
                            count++;
                        }else if(jp.contains(j+""+p))
                        {
                            int temp = getIndex(jp,j+""+p);
                            if(count_jp[temp]<K){
                            count_jp[temp]++;
                            check++;
                            }
                        }else if(js.contains(j+""+s))
                        {
                            int temp = getIndex(js,j+""+s);
                            if(count_js[temp]<K){
                            count_js[temp]++;
                            check++;
                            }
                        }else if(ps.contains(p+""+s))
                        {
                            int temp = getIndex(ps,p+""+s);
                            if(count_ps[temp]<K){
                            count_ps[temp]++;
                            check++;
                            }
                        }
                        if(check==3)
                        {
                            result.add(j+" "+p+" "+s);
                            count++;
                        }
                        check=0;
                            
                        
                        
                }
            }
                
            
            
        }
            System.out.println("Case #"+(T_i+1)+": "+count);
    
    }
}
}
