/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coinjam;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author ketan
 */
public class CoinJam {

    /**
     * @param args the command line arguments
     */
    private static long generate(long value)
    {
        Random  r = new Random();
        String result ="";
         long num = value;
        while(num>1)
        {
            long remainder = num%2;
            result =result+remainder ; 
            num = num/2;
            
        }
        while(result.length()<14)
        {
            result = result+(Math.abs(r.nextLong())%2);
        }
        result = 1+result+1;
        
        num = Long.parseLong(result);
      
        return num;
    }
    
    private static long checkPrime(long value)
    {
        long  divisor=-1L;long sqrt=0L;
        
        sqrt  = (long) Math.sqrt(value);
        //System.out.println("square root is "+sqrt);
        
        for(int i=2;i<(sqrt+1);i++)
        {
            if(value%i==0)
            {
                divisor = i;
            }
        }
        
        return divisor;
    }
    
    private static long evaluate(long number, long base)
    {
        long result =0L;long count=0L;
        
        while(number>0)
        {
            result += (long)(((long)number%10L)*((long)Math.pow(base,count)));
            count++;
            number = (long)(number/10L);
        }
        
        return result;
    }
    
    
  public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // TODO code application logic here
        int T = sc.nextInt();
        
        for(int i=0;i<T;i++){
            
            int N = sc.nextInt();
            int J = sc.nextInt();
            System.out.println("Case #1: ");
            long value = 0L,number =0L;
           // System.out.println(value);
            
            int count=0;
            ArrayList<Long> result = new ArrayList<Long>();
        
           // System.out.println("starting while loop  ");
            while(count<J)
            {  
                long[] div = new long[9];
                int x=0;
                number = generate(value);
                value = value+1;
                //System.out.println("Inside while");
                //System.out.println("after generate "+number);
        
       
                for(int j=2;j<11;j++)
                {
                    long n =evaluate(number,j);
                    //System.out.println("evaluated "+n+" for "+number+" base "+j);
                    if((div[(j-2)] = checkPrime(n))==-1)
                    {
                        //System.out.println("It is prime for "+j);
                        break;   
                    }
                    x++;
                }
                if(x==9)
                {
                    //System.out.println("It is equla to 9 "+ number);
                    if(!result.contains(number))
                    {
                        result.add(number);
                        
                        System.out.println(number+" "+div[0]+" "+div[1]+" "+div[2]+" "+div[3]+" "+div[4]+" "+div[5]
                        +" "+div[6]+" "+div[7]+" "+div[8]);
                        count++;
                    }
                }
            }
       }
   }
}
