/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mushroommonster;

import java.util.Scanner;

/**
 *
 * @author ketan
 */
public class MushroomMonster {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for(int i=0;i<T;i++)
        {
            int N = sc.nextInt();
            int[] m = new int[N];
            int temp=0,firstTotal=0,secondTotal=0,rate=0,remainder=0;
            for(int j=0;j<N;j++)
            {
                m[j] = sc.nextInt();
                if(j>0)
                {
                    temp = m[j-1]-m[j];
                    if(temp >0)
                    {
                        firstTotal += temp;
                    }
                    if(temp>rate)
                    {
                        rate = temp;
                    }
                    
                }
            }
            
           
            for(int j=0;j<N-1;j++)
            {
                int value = m[j];
                if(value>=rate)
                {
                    secondTotal += rate;
                }
                else if(value<rate)
                {
                    secondTotal += value;
                }
            }
            System.out.println("Case #"+(i+1)+": "+firstTotal+"  "+secondTotal);
        }
        
    }
    
}
